import re
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from .serializers import ArticleSerializer
from .models import Article
from rest_framework import generics
from rest_framework import mixins
from rest_framework import viewsets

# Create your views here.

class ArticleViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()