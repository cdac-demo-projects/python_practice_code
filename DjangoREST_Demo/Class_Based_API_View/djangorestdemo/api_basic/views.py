import re
from django.http.response import HttpResponse, JsonResponse, StreamingHttpResponse
from django.shortcuts import render
from django.template.response import SimpleTemplateResponse
from rest_framework import serializers
from rest_framework.views import APIView, View
from rest_framework.response import Response
from rest_framework import status
from .serializers import ArticleSerializer
from .models import Article
# Create your views here.


class TestView(View):
    def get(self, request):
        # return HttpResponse('Shivam')
        # return JsonResponse('Shivam', safe=False)
        return JsonResponse({'name': 'shivam'})
        # return Response('Shivam')   # Error : .accepted_renderer not set on Response

class TestAPIView(APIView):
    def get(self, request):
        data = [10,20,30]
        # return HttpResponse('Shivam')
        # return JsonResponse('Shivam', safe=False)
        # return JsonResponse({'name': 'shivam'})
        # return StreamingHttpResponse(data)  # Content-Type: text/html; charset=utf-8, Output : 102030
        return Response(data)

    def post(self, request):
        return Response('Response from backend')

class ArticleAPIView(APIView):

    def get(self, request):
        articles = Article.objects.all()    # query set
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializers = ArticleSerializer(data=request.data)

        if serializers.is_valid():
            serializers.save()
            return Response(serializers.data, status=status.HTTP_201_CREATED)
        return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)

class ArticleDetails(APIView):

    def get(self, request, id):
        article = self.get_article(id)
        serializer = ArticleSerializer(article)
        return Response(serializer.data)

    def put(self, request, id):
        article = self.get_article(id)
        serializer = ArticleSerializer(article, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        article = self.get_article(id)
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_article(self, id):
        try:
            return Article.objects.get(id=id)
        except Article.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)