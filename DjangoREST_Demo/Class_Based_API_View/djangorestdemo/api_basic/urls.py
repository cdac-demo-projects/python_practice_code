from django.urls import path
from . import views

urlpatterns = [
    path('article/', views.ArticleAPIView.as_view()),
    path('article/<int:id>/', views.ArticleDetails.as_view()),
    path('test/',views.TestView.as_view()),
    path('test1/',views.TestAPIView.as_view())
]