from django.urls import path
from django.urls.conf import include
from rest_framework import routers
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('article', views.ArticleViewSet, basename='article')

urlpatterns = [
    path('', include(router.urls)),
    path('<int:pk>/', include(router.urls)),
]