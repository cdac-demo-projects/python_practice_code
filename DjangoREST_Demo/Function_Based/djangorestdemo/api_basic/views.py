from json.encoder import JSONEncoder
from django.shortcuts import render
from django.http  import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from .models import Article
from django.views.decorators.csrf import csrf_exempt
from .serializers import ArticleSerializer
from rest_framework.response import Response
import json
# Create your views here.

@csrf_exempt
def test(request):
    print(request.data())
    if request.method == 'GET':
        # return 'Shivam' 
        # #
        #  Error : 'str' object has no attribute 'get'
        # #
        
        # return HttpResponse('Shivam')
        # #
        # Content-Type: text/html; charset=utf-8
        # #

        # return JsonResponse('Shivam')  
        # #
        # In order to allow non-dict objects to be serialized set the safe parameter to False
        # #

        # return JsonResponse('Shivam', safe= False)  
        # # 
        # Content-Type: application/json
        # output : "Shivam"

        return JsonResponse({'name' : 'Shivam'})    
        # #
        # Content-Type: application/json
        # output : { name : "Shivam" }
        # #

        # return Response('Shivam')
        # #
        # Error : .accepted_renderer not set on Response
        # #


@csrf_exempt
def test1(request):
    article = Article.objects.all()
    
    # return HttpResponse(article)
    # #
    # Content-Type: text/html; charset=utf-8
    # Output : Title1Title3Title3
    # #

    # return JsonResponse(article, safe=False)
    # #
    # Content-Type: text/html
    # Error : Object of type QuerySet is not JSON serializable
    # #

    return JsonResponse(json.loads('{"name" : "Shivam"}'))


@csrf_exempt
def article_list(request):
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True) 
        return JsonResponse(serializer.data, safe = False)
    elif request.method == 'POST':
        print(request)
        data = JSONParser().parse(request)
        print(data)
        serializer = ArticleSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def article_detail(request,pk):
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return HttpResponse(status=404)
    
    if request.method == 'GET':
        serializer = ArticleSerializer(article)
        return JsonResponse(serializer.data)
    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ArticleSerializer(article, data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    
    if request.method == 'DELETE':
        article.delete()
        return HttpResponse(status=204)