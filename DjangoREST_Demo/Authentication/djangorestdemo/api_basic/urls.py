from django.urls import path
from . import views

urlpatterns = [
    path('article/', views.GenericAPIView.as_view()),
    path('article/<int:id>/', views.GenericAPIView.as_view())
]