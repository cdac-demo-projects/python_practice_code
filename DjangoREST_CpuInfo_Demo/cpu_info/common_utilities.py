from re import sub
import pymongo
from datetime import datetime, timedelta
from pymongo import MongoClient
import subprocess

# To create mongo client #
def mongo_connection():
    try:
        client = MongoClient('mongodb://localhost:27017')
        db = client['test_db']
    except Exception as e:
        print(e)
    return db
db = mongo_connection()

def starttime():
    return datetime.now() - timedelta(minutes=5)

def get_cpu_info(nodename):
    """
    Retrive nodename's cpu info
    """
    # result = db.cpu_info.find( {'node':nodename, 'timestamp':{'$gt':starttime()}}, limit=1).sort('timestamp', pymongo.DESCENDING)[0]
    result = db.cpu_info.find({},{'_id':False}).sort('timestamp', pymongo.DESCENDING)
    if result.count() == 0:
        result = {}
    else:
        result = result[0]
    return result

def runOSCommands(commandarglist):
    out = subprocess.check_output(commandarglist)
    print(f'out : {out} :: type : {type(out)}') # binary type so need to convert it in string
    outArr = out.decode('utf-8').split('\n')
    output = ''
    for out in outArr:
        output += out
    return output.strip()

def get_user_info(userid):
    """
    Retrive user details using userid
    """
    user = None
    # result = db.users.find({'userid':userid},{'_id':False})[0]
    result = {
        'username' : 'shivam'
    }
    if result:
        user = result
    return user