import socket
from rest_framework.views import APIView
from rest_framework.response import Response
import common_utilities as utils
from .utilities import *
from rest_framework.permissions import AllowAny
# Create your views here.

hostname = socket.gethostname()

class GetCPUInfo(APIView):

    def get(self, request):
        cpu_info = utils.get_cpu_info(hostname)
        return Response(cpu_info)

class GetUpTime(APIView):
    permission_classes = [AllowAny]
    # Note: when you set new permission classes via the class attribute or decorators
    # you're telling the view to ignore the default list set in the settings.py file.

    def get(self, request):
        return Response(get_uptime())