import common_utilities as utils

def get_uptime():
    output = utils.runOSCommands(['uptime'])
    uptime = output.split(',')[0]
    return uptime