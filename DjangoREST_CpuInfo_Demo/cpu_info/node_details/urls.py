from django.urls import path
from .views import ( GetCPUInfo, GetUpTime )

urlpatterns = [
    path('cpu-info/', GetCPUInfo.as_view()),
    path('uptime/', GetUpTime.as_view())
]