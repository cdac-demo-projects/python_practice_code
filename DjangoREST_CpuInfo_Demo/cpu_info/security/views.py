from django.contrib.auth import authenticate, login, logout
from django.middleware.csrf import REASON_BAD_REFERER
from django.shortcuts import render
from rest_framework import settings
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework_jwt.settings import api_settings
# Create your views here.

from cpu_info import settings

app_config = settings.app_config
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

class UserLogin(APIView):
    
    permission_classes = (AllowAny,)
    def post(self, request):
        username = request.data.get('username') # Json Data
        password = request.data.get('password')
        print(f'username : {username} and password : {password}')

        user = authenticate(request,username=username,password=password)
        
        if user is not None:
            role = 'user'
            if user in app_config.ADMIN_USERS:
                role = 'admin'
            
            login(request,user)
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            return Response({
                'username': username,
                'role': role,
                'token': token
            })

        return Response(
            {
                "status":"error", 
                "message": "Invalid credentials"
            }, 
            status=status.HTTP_401_UNAUTHORIZED)

class UserLogout(APIView):

    def get(self, request):
        logout(request)
        return Response({'status' : 'success'}, status.HTTP_200_OK)