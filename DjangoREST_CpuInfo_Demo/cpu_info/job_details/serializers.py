from django.db.models import fields
from rest_framework import serializers
import time
import common_utilities as utils

from .models import SlurmJobTable


class SlurmJobTableSerializer(serializers.ModelSerializer):
    userdetail = serializers.SerializerMethodField()
    status = serializers.CharField()

    def to_representation(self, instance):
        representation = super(SlurmJobTableSerializer, self).to_representation(instance)
        representation['time_end'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(instance.time_end))
        representation['time_start'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(instance.time_start))
        return representation
        
    class Meta:
        model = SlurmJobTable
        fields = ('id_job','time_start','time_end','job_name','mod_time','userdetail','nodes_alloc','nodelist','status','exit_code')
        # fields = '__all__'

    def get_userdetail(self, obj):
        userinfo = utils.get_user_info(obj.id_user)
        return userinfo