from django.urls import path

from .views import (
    RunningJobsListApiView,
    SelectAllJobListApiView,
    Squeue,
)

urlpatterns = [
    path('jobs/running/',  RunningJobsListApiView.as_view()),
    path('selectjobs/<str:state>/',  SelectAllJobListApiView.as_view()),
    path('squeue', Squeue.as_view()),
]
