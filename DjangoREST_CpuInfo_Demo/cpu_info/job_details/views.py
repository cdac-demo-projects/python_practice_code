from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from django.db.models import *
import subprocess as sp
import json

from .models import SlurmJobTable
from .serializers import SlurmJobTableSerializer

# Create your views here.

class RunningJobsListApiView(generics.ListAPIView):
    serializer_class = SlurmJobTableSerializer

    queryset = SlurmJobTable.objects.annotate(
        status=Case(
            When(state=3, then=Value('Completed')),
            When(state=5, then=Value('Failed')),
            When(state=1, then=Value('Running')),
            When(state=0, then=Value('Pending')),
            default=Value('Unknown'),
            output_field=CharField(),
            ),).filter(status="Running")
    # queryset = SlurmJobTable.objects.all()[:5]

class SelectAllJobListApiView(generics.ListAPIView):
        serializer_class = SlurmJobTableSerializer

        def get_queryset(self):
            state = self.kwargs['state']
            queryset = SlurmJobTable.objects.annotate(
                status=Case(
                    When(state=3, then=Value('Completed')),
                    When(state=0, then=Value('Pending')),
                    When(state=5, then=Value('Failed')),
                    When(state=4, then=Value('Failed')),
                    When(state=1, then=Value('Running')),
                    When(state=0, then=Value('Running')),
                    default=Value('Unknown'),
                    output_field=CharField(),
                    )).filter(status=str(state))
            return queryset

class SelectedJobListApiView(generics.ListAPIView):
        serializer_class = SlurmJobTableSerializer
        def get_queryset(self):
                userid = self.kwargs['id']
                state = self.kwargs['state']
                serializer_class = SlurmJobTableSerializer
                queryset = SlurmJobTable.objects.annotate(status=Case(
                    When(state=3, then=Value('Completed')),
                    When(state=0, then=Value('Pending')),
                    When(state=5, then=Value('Failed')),
                    When(state=4, then=Value('Failed')),
                    When(state=1, then=Value('Running')),
                    When(state=0, then=Value('Running')),
                    default=Value('Unknown'),output_field=CharField(),),).filter(status=str(state),id_user=userid)
                return queryset

# Alternate method for SelectedJobListApiView
class SelectedStateUserJobList(APIView):
    def post(self, request):
        userid = request.data.get('userid')
        state =  request.data.get('state')
        jobs = SlurmJobTable.objects.annotate(status=Case(
            When(state=3, then=Value('Completed')),
            When(state=0, then=Value('Pending')),
            When(state=5, then=Value('Failed')),
            When(state=4, then=Value('Failed')),
            When(state=1, then=Value('Running')),
            When(state=0, then=Value('Running')),
            default=Value('Unknown'),
            output_field=CharField(),),).filter(status=str(state),id_user=userid)
        serializer = SlurmJobTableSerializer(jobs, many=True)
        return Response(serializer.data)

class Squeue(APIView):

    def get(self,request):
        process = sp.Popen(['squeue', '-o', '%all'], stdout = sp.PIPE, stderr = sp.PIPE, shell = False, universal_newlines = True)
        proc_stdout, proc_stderr = process.communicate()
        lines = proc_stdout.split('\n')
        header_line = lines.pop(0)
        header_cols = header_line.split('|')
        entries = []
        error_lines = [] # do something with this later
        for line in lines:
            parts = line.split('|')
            d = {}
            print(len(parts))
            print(len(header_cols))
            if len(parts) != len(header_cols):
                error_lines.append((len(parts), line, parts))
            else:
                for i, key in enumerate(header_cols):
                    d[key] = parts[i]
            entries.append(d)
        return Response(entries)