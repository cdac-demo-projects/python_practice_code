# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from cpu_info import settings

app_conf = settings.app_config

table_prefix = app_conf.DB_TABLE_PREFIX

class SlurmJobTable(models.Model):
    job_db_inx = models.BigAutoField(primary_key=True)
    mod_time = models.PositiveBigIntegerField()
    deleted = models.IntegerField()
    account = models.TextField(blank=True, null=True)
    admin_comment = models.TextField(blank=True, null=True)
    array_task_str = models.TextField(blank=True, null=True)
    array_max_tasks = models.PositiveIntegerField()
    array_task_pending = models.PositiveIntegerField()
    constraints = models.TextField(blank=True, null=True)
    cpus_req = models.PositiveIntegerField()
    derived_ec = models.PositiveIntegerField()
    derived_es = models.TextField(blank=True, null=True)
    exit_code = models.PositiveIntegerField()
    flags = models.PositiveIntegerField()
    job_name = models.TextField()
    id_assoc = models.PositiveIntegerField()
    id_array_job = models.PositiveIntegerField()
    id_array_task = models.PositiveIntegerField()
    id_block = models.TextField(blank=True, null=True)
    id_job = models.PositiveIntegerField()
    id_qos = models.PositiveIntegerField()
    id_resv = models.PositiveIntegerField()
    id_wckey = models.PositiveIntegerField()
    id_user = models.PositiveIntegerField()
    id_group = models.PositiveIntegerField()
    het_job_id = models.PositiveIntegerField()
    het_job_offset = models.PositiveIntegerField()
    kill_requid = models.IntegerField()
    state_reason_prev = models.PositiveIntegerField()
    mcs_label = models.TextField(blank=True, null=True)
    mem_req = models.PositiveBigIntegerField()
    nodelist = models.TextField(blank=True, null=True)
    nodes_alloc = models.PositiveIntegerField()
    node_inx = models.TextField(blank=True, null=True)
    partition = models.TextField()
    priority = models.PositiveIntegerField()
    state = models.PositiveIntegerField()
    timelimit = models.PositiveIntegerField()
    time_submit = models.PositiveBigIntegerField()
    time_eligible = models.PositiveBigIntegerField()
    time_start = models.PositiveBigIntegerField()
    time_end = models.PositiveBigIntegerField()
    time_suspended = models.PositiveBigIntegerField()
    gres_req = models.TextField()
    gres_alloc = models.TextField()
    gres_used = models.TextField()
    wckey = models.TextField()
    work_dir = models.TextField()
    system_comment = models.TextField(blank=True, null=True)
    track_steps = models.IntegerField()
    tres_alloc = models.TextField()
    tres_req = models.TextField()

    class Meta:
        managed = False
        # db_table = 'shreshta_job_table'
        db_table = table_prefix + '_job_table'
        unique_together = (('id_job', 'time_submit'),)

    def save(self, *args, **kwargs):
        return

    def delete(self, *args, **kwargs):
        return