#!/home/shivam/Shivam_Palaskar/python_practice_code/DjangoREST_CpuInfo_Demo/billing/myvenv/bin/python

from utilities import *
from datetime import datetime
import argparse
from utilities import *
from utilities import app_config


from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

# Date and time formatting
today = datetime.today()
today_str = str(today.strftime("%d %b %Y"))
today_short = str(today.strftime("%d%m%y"))
lw_start, lw_end = previous_week_range(today)

def parse_args():
    parser = argparse.ArgumentParser(
        prog='create_usage_bill.py', description='Creates a usage bill for a user ')
    parser.add_argument(
        '-m', '--last_month', action='store_true', default=False, help="If specified last month usage will be calculated.")
    parser.add_argument(
        '-w', '--last_week', action='store_true', default=True, help="[DEFAULT] If specified last week usage will be calculated.")
    parser.add_argument(
        '-u', '--username', action='store', help="[REQUIRED] Specify username for whom usage to be calculated.", required=True)

    return parser.parse_args()


args = parse_args()     # Namespace(last_month=False, last_week=True, username='shivam')


# Registering font styles
pdfmetrics.registerFont(TTFont(
    'Ubuntu', app_config.APP_HOME + 'billing/fonts/ubuntu/Ubuntu-R.ttf'))
pdfmetrics.registerFont(TTFont(
    'Ubuntu-Bold', app_config.APP_HOME + 'billing/fonts/ubuntu/Ubuntu-B.ttf'))
pdfmetrics.registerFont(TTFont(
    'Silkscreen', app_config.APP_HOME + 'billing/fonts/Silkscreen/slkscr.ttf'))

# Generate invoice PDF using above data
def generate_invoice(bill_details):
    # Creating Canvas
    runStringCommand(
        'mkdir -p ' + app_config.APP_HOME + 'billing/invoices')
    invoice_number = 1234
    INVOICE_PATH = app_config.APP_HOME+"billing/invoices/" + \
        today_short + '_' + str(invoice_number) + \
        '_' + bill_details['username'] + "_usage_invoice.pdf"
    c = canvas.Canvas(INVOICE_PATH, pagesize=(400, 500), bottomup=0)

    # Logo Section
    c.translate(10, 50)
    # Inverting the scale for getting mirror Image of logo
    c.scale(1, -1)
    c.drawImage(app_config.APP_HOME +
                "billing/images/cdac-logo.png", 0, 0, width=40, height=40)

    # Title Section
    c.scale(1, -1)
    # Again Setting the origin back to (0,0) of top-left
    c.translate(-10, -50)
    # Setting the font for Name title of company
    c.setFont("Ubuntu-Bold", 12.5)
    c.drawCentredString(
        200, 25, "Centre for Development of Advanced Computing")
    c.setFont("Ubuntu", 8)
    c.drawCentredString(210, 35, "Innovation Park, Mansarovar, Panchwati")
    c.drawCentredString(210, 45, "Pashan, Pune (IN) - 411008")
    c.line(10, 60, 390, 60)

    # Logo Section
    c.translate(350, 50)
    # Inverting the scale for getting mirror Image of logo
    c.scale(1, -1)
    c.drawImage(app_config.APP_HOME +
                "billing/images/nsm-logo.png", 0, 0, width=40, height=40)

    # Title Section
    c.scale(1, -1)
    # Again Setting the origin back to (0,0) of top-left
    c.translate(-350, -50)
    # Document Information
    # Changing the font for Document title
    c.setFont("Ubuntu-Bold", 10)
    c.drawCentredString(200, 75, "INVOICE")

    c.setFont("Ubuntu", 8)
    c.drawString(10, 85, "Invoice No.: " + str(1234))
    c.drawCentredString(350, 85, "Date: " + today_str)

    # FIRST Block Consist of User details
    c.roundRect(10, 90, 380, 105, 1, stroke=1, fill=0)

    c.setFont("Ubuntu-Bold", 8)
    c.drawString(20, 110, 'Bill To')
    c.setFont("Ubuntu", 8)
    c.drawString(20, 120, bill_details['username'] + ' ('+bill_details['account']+'),')

    c.setFont("Ubuntu-Bold", 8)
    c.drawString(20, 130, 'Email : ')
    c.setFont("Ubuntu", 8)
    c.drawString(50, 130, bill_details['email'])

    # sEcoND Block Consist of consumption Details
    c.roundRect(10, 200, 270, 150, 1, stroke=1, fill=0)
    c.roundRect(285, 200, 105, 150, 1, stroke=1, fill=0)

    c.setFont("Ubuntu-Bold", 8)
    c.drawCentredString(
        135, 210, 'Usage Summary For - ' + bill_details['invoice_duration'])

    # Table
    c.line(10, 215, 280, 215)
    c.line(10, 250, 280, 250)
    c.line(10+54*3, 215, 10+54*3, 250)
    c.line(10, 285, 280, 285)
    c.line(10+54*3, 250, 10+54*3, 285)

    c.setFont("Ubuntu", 8)
    c.drawString(15, 235, 'CPU Compute Consumed')
    c.setFont("Ubuntu-Bold", 8)
    c.drawString(177, 235,str(bill_details['cpumins']))
    c.setFont("Ubuntu", 6)
    c.drawRightString(275, 235, '(CPU Core Mins.)')

    c.setFont("Ubuntu", 8)
    c.drawString(15, 270, 'GPU Compute Consumed')
    c.setFont("Ubuntu-Bold", 8)
    c.drawString(177, 270, str(bill_details['gpumins']))
    c.setFont("Ubuntu", 6)
    c.drawRightString(275, 270, '(GPU Card Mins.)')

    # Jobs Processed
    c.setFont("Ubuntu-Bold", 10)
    c.drawString(15, 340, 'No. of Jobs Proccessed : ' + str( bill_details['njobs'] ))

     # Consumption in INR
    c.setFont("Ubuntu", 8)
    c.drawString(295, 220, 'eMoney Consumed')
    c.line(285, 315, 390, 315)
    c.setFont("Ubuntu-Bold", 12)
    c.drawString(295, 330, inr_sym)
    c.setFont("Silkscreen", 12)
    c.drawString(305, 330,  str(bill_details['emoney_consumed']))
    c.setFont("Ubuntu", 6)

    # Signature
    c.setFont("Ubuntu", 8)
    c.drawString(
        10, 400, '(This is a computer generated invoice and no signature is required.)')
    c.setFont("Ubuntu-Bold", 8)
    c.drawString(10, 410, 'Authorized Signatory')

    c.line(10, 430, 390, 430)

    # References
    c.setFont("Ubuntu", 8)
    c.drawString(10, 440, f"Per CPU Core/Minutes Charges = {bill_details['percpu']} paisa")
    c.drawString(10, 450, f"Per GPU Card/Minutes Charges = {bill_details['pergpu']} paisa")

    # End the Page and Start with new
    c.showPage()
    # Saving the PDF
    c.save()

bill_details = {'invoice_number': str(today.year) + '/' + str(today.month) + '/' + f'{ 1234 :04}',
                    'username': args.username,
                    'invoice_duration': '5/30/21 - 6/30/21',
                    'percpu': 10,
                    'pergpu': 10,
                    'cpumins': 10,
                    'gpumins': 10,
                    'njobs': 10,
                    'emoney_consumed': 10,
                    'email': 'shivam@gmail.com',
                    'account': 'hpctech',
                    'date': datetime.now(), }
generate_invoice(bill_details)