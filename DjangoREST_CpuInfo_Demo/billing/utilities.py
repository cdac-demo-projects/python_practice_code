from datetime import timedelta
import os, imp, subprocess

start_sqlformat = '%Y-%m-%d 00:00:00'
end_sqlformat = '%Y-%m-%d 23:59:59'
inr_sym = u"\u20B9"

def previous_week_range(date):
    start_date = date + timedelta(-date.weekday(), weeks=-1)
    start_date.replace(hour=0, minute=0, second=0, microsecond=0)
    end_date = date + timedelta(-date.weekday() - 1)
    end_date.replace(hour=23, minute=59, second=59, microsecond=0)
    return start_date, end_date

ROOT_PATH = os.environ['ROOT_PATH']
app_config = None
filepath = ROOT_PATH + '/.secret/admin_config.py'
with open(filepath,'rb') as fp:
    temp_conf = imp.load_module('app_config', fp, filepath, ('.py', 'rb', imp.PY_SOURCE))
app_config = temp_conf

def runStringCommand(command):
    try:
        data = subprocess.check_output(
            command,  stderr=subprocess.STDOUT, shell=True)
        return data.decode('utf-8')
    except subprocess.CalledProcessError as e:
        return e.returncode