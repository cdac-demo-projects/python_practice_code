#!/home/shivam/Shivam_Palaskar/python_practice_code/DjangoREST_CpuInfo_Demo/cpu_info/myvenv/bin/python

import socket,psutil
from datetime import datetime, timedelta
from pymongo import MongoClient
from timeloop import Timeloop

# To create mongo client #
def mongo_connection():
    try:
        client = MongoClient('mongodb://localhost:27017')
        db = client['test_db']
    except Exception as e:
        print(e)
    return db
db = mongo_connection()

interval=None
tl =  Timeloop()

hostname = socket.gethostname()

# CPU Percent
def get_cpu_percent():
    percent = psutil.cpu_percent(interval=interval)
    return percent

# total cpu utilzations
def get_total_cpu_utilizations():
    total_cpu_times = psutil.cpu_times_percent(interval=interval)
    total_cpu_util = {
        "user": total_cpu_times.user,
        "system": total_cpu_times.system,
        "idle": total_cpu_times.idle
    }
    return total_cpu_util

# cpu per core utilizations
def get_core_utilizations():
    count = 0
    core_utilizations = []
    for proc in psutil.cpu_times_percent(interval=interval, percpu=True):
        count += 1
        total = proc.user+proc.system+proc.idle+proc.guest_nice + \
            proc.guest+proc.steal+proc.softirq+proc.irq+proc.iowait+proc.nice
        temp = {
            'user': round(proc.user*100/total, 2),
            'system': round(proc.system*100/total, 2),
            'idle': round(proc.idle*100/total, 2),
            'core': 'cpu' + str(count)
        }
        core_utilizations.append(temp)
    return core_utilizations

# Cpu Statistics
def get_cpu_stats():
    temp = psutil.cpu_stats()

    cpuStat = {
        'ctx_switches': temp.ctx_switches,
        'interrupts': temp.interrupts,
        'soft_interrupts': temp.soft_interrupts,
        'syscalls': temp.syscalls,
    }
    return cpuStat

@tl.job(interval=timedelta(seconds=5))
def cpu_info():
    """ 
    cpu info to collections cpu_info, insert, 10s 
    """
    cpu_info = db.cpu_info
    result = cpu_info.insert_one({
        "node": hostname,
        "timestamp": datetime.now(),
        "cpu_percent": get_cpu_percent(),
        "core_utilizations": get_core_utilizations(),
        "total_cpu_util": get_total_cpu_utilizations(),
        "cpu_stats": get_cpu_stats()
    })
    print("New Data Updated at : "+datetime.now().strftime("%d/%m/%Y %H:%M:%S"))



if __name__ == "__main__":
    """
    Will automatically shut down the jobs(worker threads) gracefully when the program is killed,
    so no need to call tl.stop() 
    """
    tl.start(block=True)