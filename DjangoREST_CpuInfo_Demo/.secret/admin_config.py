import os

ADMIN_USERS = ['root','cadmin']

SCHEDULER = 'slurm'
SCHEDULER_DB_NAME = 'slurm_acct_db'
DB_TABLE_PREFIX = 'shreshta'
__DB_USERNAME = 'root'
__DB_PASSWORD = 'root'
DB_SERVER = 'localhost'

APP_HOME = '/home/shivam/Shivam_Palaskar/python_practice_code/DjangoREST_CpuInfo_Demo/'