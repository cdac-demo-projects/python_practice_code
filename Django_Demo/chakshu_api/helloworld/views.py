from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.

def say_hello(request):
    # return HttpResponse('<h2>Hello World</h2>')
    # return render(request, 'hello.html')
    name = 'Shivam Palaskar'
    return render(request, 'hello.html',{'name' : name})