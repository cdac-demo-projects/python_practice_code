from . import views
from django.urls.conf import path


urlpatterns = [
    path('register',views.register),
    path('login',views.login),
    path('logout',views.logout)
]