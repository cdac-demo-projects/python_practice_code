from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth.models import User, auth
# Create your views here.

def register(request):

    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']

        if User.objects.filter(username=username).exists():
            messages.info(request,'Username already exists')
            return HttpResponseRedirect('register')
        else :
            user =User.objects.create_user(username=username, password=password, email=email, first_name=first_name, last_name=last_name)
            user.save()
        return redirect('/travello')
    else:
        return render(request,'register.html')

def login(request):
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/travello')
        else:
            messages.info(request, 'Invalid Credentials')
            return HttpResponseRedirect('login')
            # return redirect('login')    Error :NoReverseMatch
    else :
        print('GET')
        return render(request,'login.html')

def logout(request):
    auth.logout(request)
    return redirect('/travello')