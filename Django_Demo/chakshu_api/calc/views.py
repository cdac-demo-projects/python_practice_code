from django.shortcuts import render

# Create your views here.

def addition(request):
    # num1 = int(request.GET['num1'])
    # num2 = int(request.GET['num2'])
    num1 = int(request.POST['num1'])
    num2 = int(request.POST['num2'])
    return render(request, 'calc_result.html', {'res' : num1+num2})

def home(request):
    return render(request,'calc.html')