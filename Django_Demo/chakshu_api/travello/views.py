from django.shortcuts import render
from .models import Destination

# Create your views here.

def home(request):

    dest1 = Destination()
    dest1.name = 'Mumbai'
    dest1.desc = 'City Of Dreams'
    dest1.img = 'destination_1.jpg'
    dest1.price = 899
    dest1.offer = False

    dest2 = Destination()
    dest2.name = 'Delhi'
    dest2.desc = 'Capital City'
    dest2.img = 'destination_2.jpg'
    dest2.price = 999
    dest2.offer = True

    dest3 = Destination()
    dest3.name = 'Pune'
    dest3.desc = 'Historic City'
    dest3.img = 'destination_3.jpg'
    dest3.price = 799
    dest3.offer = False

    # dests = [dest1, dest2, dest3]

    dests = Destination.objects.all()

    return render(request, 'index.html', {'dests' : dests})