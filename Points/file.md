## Serializer

- DRF’s Serializers convert model instances to Python dictionaires


## Request

- As REST framework's `Request` extends Django's `HttpRequest`
- REST framework's `Request` class extends the standard `HttpRequest`, adding support for REST framework's flexible request parsing and request authentication.

## Response

- The `Response` class subclasses Django's `SimpleTemplateResponse`. Response objects are initialised with data, which should consist of native Python primitives. REST framework then uses standard HTTP content negotiation to determine how it should render the final response content.
- There's no requirement for you to use the Response class, you can also return regular `HttpResponse` or `StreamingHttpResponse` objects from your views if required.
- **Using the Response class simply provides a nicer interface for returning content-negotiated Web API responses, that can be rendered to multiple formats.**
- Unless you want to heavily customize REST framework for some reason, you should always use an APIView class or @api_view function for views that return Response objects.
- We can send primite data of python through Response
- **The renderers used by the Response class cannot natively handle complex datatypes such as Django model instances, so you need to serialize the data into primitive datatypes before creating the Response object.**