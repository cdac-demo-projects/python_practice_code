#!/home/shivam/Shivam_Palaskar/python_practice_code/PyMongo_Demo/myvenv/bin/python

import time
import utilities
from timeloop import Timeloop
from datetime import timedelta

db = utilities.mongo_connection()

tl = Timeloop()

@tl.job(interval=timedelta(seconds=2))
def add_user():
    user = {'name' : 'Shivam'}
    user_id = db.users.insert_one(user).inserted_id
    if user_id is not None:
        print(f'user inserted successfully : {user_id}')

# @tl.job(interval=timedelta(seconds=2))
# def sample_job_every_2s():
#     print("Hello")

if __name__ == "__main__":
    """
    Will automatically shut down the jobs(worker threads) gracefully when the program is killed,
    so no need to call tl.stop() 
    """
    tl.start(block=True)