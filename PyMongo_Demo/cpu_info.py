from pymongo import results
import utilities
import socket
from datetime import datetime, timedelta
import psutil, pymongo

db = utilities.mongo_connection()

hostname = socket.gethostname()

# CPU Percent
def get_cpu_percent():
    percent = psutil.cpu_percent(interval=10)
    return percent

# total cpu utilzations
def get_total_cpu_utilizations():
    total_cpu_times = psutil.cpu_times_percent(interval=5)
    total_cpu_util = {
        "user": total_cpu_times.user,
        "system": total_cpu_times.system,
        "idle": total_cpu_times.idle
    }
    return total_cpu_util

# cpu per core utilizations
def get_core_utilizations():
    count = 0
    core_utilizations = []
    for proc in psutil.cpu_times_percent(interval=10, percpu=True):
        count += 1
        total = proc.user+proc.system+proc.idle+proc.guest_nice + \
            proc.guest+proc.steal+proc.softirq+proc.irq+proc.iowait+proc.nice
        temp = {
            'user': round(proc.user*100/total, 2),
            'system': round(proc.system*100/total, 2),
            'idle': round(proc.idle*100/total, 2),
            'core': 'cpu' + str(count)
        }
        core_utilizations.append(temp)
    return core_utilizations

# Cpu Statistics
def get_cpu_stats():
    temp = psutil.cpu_stats()

    cpuStat = {
        'ctx_switches': temp.ctx_switches,
        'interrupts': temp.interrupts,
        'soft_interrupts': temp.soft_interrupts,
        'syscalls': temp.syscalls,
    }
    return cpuStat

def cpu_info():
    """ 
    cpu info to collections cpu_info, insert, 5s 
    """
    cpu_info = db.cpu_info
    result = cpu_info.insert_one({
        "node": hostname,
        "timestamp": datetime.now(),
        "cpu_percent": get_cpu_percent(),
        "core_utilizations": get_core_utilizations(),
        "total_cpu_util": get_total_cpu_utilizations(),
        "cpu_stats": get_cpu_stats()
    })
    return result

def starttime():
    return datetime.now() - timedelta(minutes=5)

def get_cpu_percent(nodename):
    """
    Retrive nodename's cpu_percent
    """
    cpu_percent = 0.0
    result = db.cpu_info.find( {'node': nodename, 'timestamp' : {'$gt': starttime() }})
    print(result)
    for r in result:
        print(r)
    # result = db.cpu_info.find( {'node':nodename, 'timestamp':{'$gt':starttime()}}, limit=1).sort('timestamp', pymongo.DESCENDING)[0]
    # if result:
    #     cpu_percent = result['cpu_percent']
    # return cpu_percent

get_cpu_percent('thinkpad')
