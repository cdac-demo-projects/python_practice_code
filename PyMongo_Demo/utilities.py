from pymongo import MongoClient
import pymongo

# To create mongo client #
def mongo_connection():
    try:
        client = MongoClient('mongodb://localhost:27017')
        db = client['test_db']
    except Exception as e:
        print(e)
    return db